from django.apps import AppConfig


class AadharauthappConfig(AppConfig):
    name = 'aadharAuthApp'
