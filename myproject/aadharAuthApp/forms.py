from django import forms
from django.contrib.auth.models import User
from .models import Parties_list

class PartiesListInfoForm(forms.ModelForm):
    class Meta():
        model = Parties_list
        fields = ('party_name','party_icon')

class AddharAuthForm(forms.Form):
    aadhar_id = forms.IntegerField(label='Aadhar id',help_text="Enter a valid aadhar id",min_value=100000000000,max_value=999999999999)
    #error_messages = {'required': 'Please enter 12 digit valid aadhar number!'},

class VoterAuthForm(forms.Form):
    voter_id = forms.IntegerField(label='voter id', help_text="Enter a valid voter id", max_value=999999999999,
                                   min_value=000000000000)


class UserForm(forms.Form):
    password = forms.CharField(widget=forms.PasswordInput())
    class Meta():
        model = User
        fields = ('username','email','password')