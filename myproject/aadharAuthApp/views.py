from django.shortcuts import render,redirect
from .forms import AddharAuthForm,VoterAuthForm
from .models import user_db
from .models import Parties_list
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from base64 import b64decode
from django.contrib import messages

@login_required
def special(request):
    # Remember to also set login url in settings.py!
    # LOGIN_URL = '/basic_app/user_login/'
    return HttpResponse("You are logged in. Nice!")

@login_required
def user_logout(request):
    # Log out the user.
    logout(request)
    # Return to homepage.
    return HttpResponseRedirect(reverse('index'))

@login_required
def get_party_list(request):
    print(Parties_list.objects.all())
    party_list = Parties_list.objects.all()
    print("party_list",party_list)
    aadhar_id = request.session['aadhar_id']
    update_voting_status(aadhar_id)
    return render(request,'aadharAuthApp/party_list.html',{"party_list":party_list})



def user_login(request):

    if request.method == 'POST':
        # First get the username and password supplied
        username = request.POST.get('username')
        password = request.POST.get('password')

        # Django's built-in authentication function:
        user = authenticate(username=username, password=password)

        # If we have a user
        if user:
            #Check it the account is active
            if user.is_active:
                # Log the user in.
                login(request,user)
                # Send the user back to some page.
                # In this case their homepage.
                return HttpResponseRedirect(reverse('aadhar_data'))
            else:
                # If account is not active:
                return HttpResponse("Your account is not active.")
        else:
            print("Someone tried to login and failed.")
            print("They used username: {} and password: {}".format(username,password))
            return HttpResponse("Invalid login details supplied.")

    else:
        #Nothing has been provided for username or password.
        return render(request, 'aadharAuthApp/login.html', {})

@login_required
def get_aadhar_data(request):

    if request.method == "POST":
        try:

                aadhar_id =request.POST.get('aadhar_id')
                print(aadhar_id)
                user_db_obj = user_db.objects.get(aadhar_id=aadhar_id)
                print(user_db_obj)
                print("inside try")
                if str(user_db_obj)=="user_db object":
                    request.session['aadhar_id'] = aadhar_id
                    return HttpResponseRedirect(reverse('voter_data'))
        except Exception as e:
            print('inside exception')
            print(e)

            return render(request,'aadharAuthApp/aadhar_auth.html')
    else:
        return render(request, 'aadharAuthApp/aadhar_auth.html')

@login_required
def get_voter_data(request):
    if request.method == "POST":
        try:
            voter_id = request.POST.get('voter_id')
            print("voter_id====",voter_id)
            print("user_db.objects.filter(voter_id=voter_id)",user_db.objects.filter(voter_id=voter_id).values('voted'))

            aadhar_id=user_db.objects.filter(voter_id=voter_id).values('aadhar_id')[0]['aadhar_id']
            voted_status=user_db.objects.filter(voter_id=voter_id).values('voted')

            if int(request.session['aadhar_id']) == int(aadhar_id):
                if voted_status[0]['voted'] == 'Null':
                    return HttpResponseRedirect(reverse('party_list'))
                else:
                    return HttpResponse('Already Voted')
            else:
                messages.error(request, 'Aadhar id and Voter id did not match')
                return HttpResponseRedirect(reverse('voter_data'))

        except Exception as e:
            messages.error(request, 'Aadhar id and Voter id did not match')
            return HttpResponseRedirect(reverse('voter_data'))

    else:
        return render(request, 'aadharAuthApp/voter_auth.html')

def update_voting_status(aadhar_id):
    try:
        user_db.objects.filter(aadhar_id = aadhar_id).update(voted="Yes")
        print("Hiiiiiii")
        return HttpResponseRedirect('Your Vote is Recorded. Thanks for Voting')
    except:
        return HttpResponseRedirect(reverse('login'))

