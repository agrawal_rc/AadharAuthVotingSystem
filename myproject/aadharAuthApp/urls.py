from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^aadhar_auth/$', views.get_aadhar_data, name='aadhar_data'),
    url(r'^voter_auth/$', views.get_voter_data, name='voter_data'),
    url(r'^voter_auth/$', views.get_voter_data, name='voter_data'),
    url(r'^partylist/$', views.get_party_list, name='party_list'),
    url(r'^logout/$', views.user_logout, name='logout'),
    url(r'^accounts/login/$', views.user_login, name='login'),

]